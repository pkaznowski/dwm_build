#!/bin/sh

rm config.h 2>/dev/null

if make; then
    sed -i 's/#define MODKEY Mod1Mask/#define MODKEY Mod4Mask/' config.h
    sed -i 's/|Mod4Mask/|Mod1Mask/g' config.h
fi
